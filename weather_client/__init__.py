import io
import traceback
import urllib
from collections import namedtuple
from contextlib import closing
from datetime import datetime

from PIL import Image
import requests


class Weather:

    def __init__(self, api_key, location):
        self.api_key = api_key
        self.location = location

    Forecast = namedtuple('Forecast', 'forecast rain_amount rain_prob')
    DayForecast = namedtuple('DayForecast', 'day forecast min max')

    def get_weather(self):
        try:
            forecast = requests.get(self._url('rainfall')).json()
            precis = forecast['regionPrecis']['days'][0]['entries'][0]['precis']
            rainfall = forecast['forecasts']['rainfall']['days'][0]['entries']
        except:
            traceback.print_exc()
            precis = 'Error in get_weather()'
            rainfall = [{'rangeCode': '0', 'probability': 100}]

        return self.Forecast(precis,
                             rainfall[0]['rangeCode'],
                             rainfall[0]['probability'])

    def get_temperatures(self):
        try:
            forecast = requests.get(self._url('temperature')).json()
            entries = forecast['forecasts']['temperature']['days'][0]['entries']
            return [entry['temperature'] for entry in entries]
        except:
            traceback.print_exc()
            return []

    def _url(self, forecasts='rainfall', days=1):
        return ('https://api.willyweather.com.au/v2/'
                '{}/locations/{}/weather.json?'
                'forecasts={}&regionPrecis=true&days={}'
                .format(self.api_key, self.location, forecasts, days))

    def any_rain(self, threshold):
        return self.get_weather().rain_prob >= threshold

    def get_week_weather(self):
        try:
            forecast = requests.get(self._url('weather', days=7)).json()
            return [self._day_forecast(day['entries'][0],
                                       weather['entries'][0])
                    for day, weather
                    in zip(forecast['regionPrecis']['days'],
                           forecast['forecasts']['weather']['days'])]
        except:
            traceback.print_exc()
            return [self.DayForecast('', 'Error in get_week_weather()', 0, 0)]

    def _day_forecast(self, regionPrecis, weather):
        return self.DayForecast(
            datetime.strptime(regionPrecis['dateTime'], '%Y-%m-%d %H:%M:%S')
            .strftime('%a'),
            regionPrecis['precis'],
            weather['min'],
            weather['max'])


class Radar:

    def __init__(self, radars, request_mode='http'):
        self.radars = radars
        self.request_mode = request_mode

    def urls(self, _now=None):
        if self.request_mode == 'ftp':
            return ['ftp://ftp.bom.gov.au/anon/gen/radar/{}.gif'
                    .format(radar)
                    for radar in self.radars]
        else:
            return ['http://www.bom.gov.au/radar/{}.gif?{}'
                    .format(radar,
                            _now or datetime.utcnow().strftime('%Y%m%d%H%M%S'))
                    for radar in self.radars]

    def any_rain(self, cutoff, crop=False, _now=None):
        radar_img = self.radar_image(self.urls(_now=_now))

        if radar_img:
            if crop:
                # check for rain in area around home
                return self.rain_pixel_count(
                    radar_img.crop((220, 220, 340, 340)),
                    box_size=0) > (cutoff / 20)


            else:
                # check for rain on whole image
                return self.rain_pixel_count(radar_img) > cutoff

        else:
            return True

    @staticmethod
    def radar_image(urls):  # pragma: no cover
        for url in urls:
            if url.lower().startswith('ftp'):
                try:
                    with closing(urllib.request.urlopen(url)) as r:
                        return Image.open(r).convert('RGB')
                except urllib.error.URLError:
                    traceback.print_exc()
            else:
                try:
                    res = requests.get(url, stream=True)
                    res.raise_for_status()
                except requests.exceptions.RequestException:
                    traceback.print_exc()
                else:
                    return Image.open(io.BytesIO(res.content)).convert('RGB')

        return None

    @classmethod
    def rain_pixel_count(cls, radar_img, box_size=25 * 18):
        count = cls.rain_pixel_tally(radar_img)
        return sum(count) - (len(count) * box_size)

    @staticmethod
    def rain_pixel_tally(radar_img):
        count = {(245, 245, 255): 0,
                 (180, 180, 255): 0,
                 (120, 120, 255): 0,
                 (20, 20, 255): 0,
                 (0, 216, 195): 0,
                 (0, 150, 144): 0,
                 (0, 102, 102): 0,
                 (255, 255, 0): 0,
                 (255, 200, 0): 0,
                 (255, 150, 0): 0,
                 (255, 100, 0): 0,
                 (255, 0, 0): 0,
                 (200, 0, 0): 0,
                 (120, 0, 0): 0,
                 (40, 0, 0): 0}

        for pixel in radar_img.getdata():
            if pixel in count:
                count[pixel] += 1

        return count.values()
