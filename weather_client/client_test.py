import os
import pytest

from PIL import Image
from unittest.mock import Mock, patch

import weather_client as client


def test_weather_url():
    assert client.Weather('api_key', 'loc')._url() \
        == ('https://api.willyweather.com.au/v2/api_key/'
            'locations/loc/weather.json'
            '?forecasts=rainfall&regionPrecis=true&days=1')


def test_weather_temperature_url():
    assert client.Weather('api_key', 'loc')._url(forecasts='temperature') \
        == ('https://api.willyweather.com.au/v2/api_key/'
            'locations/loc/weather.json'
            '?forecasts=temperature&regionPrecis=true&days=1')


@patch.object(client, 'requests')
def test_get_weather(requests):
    result = Mock()
    result.json.return_value \
        = {'regionPrecis': {
            'name': 'Melbourne Area',
            'issueDateTime': '2017-08-22 18:41:29',
            'days': [{'entries': [{'precis': 'Partly cloudy. Light winds.',
                                   'dateTime': '2017-08-22 00:00:00'}],
                      'dateTime': '2017-08-22 00:00:00'}]},
           'forecasts': {
               'rainfall': {
                   'units': {'amount': 'mm'},
                   'issueDateTime': '2017-08-22 21:19:12',
                   'days': [{'entries': [{'endRange': 5,
                                          'dateTime': '2017-08-22 00:00:00',
                                          'rangeDivide': '-',
                                          'rangeCode': '1-5',
                                          'startRange': 1,
                                          'probability': 5}],
                             'dateTime': '2017-08-22 00:00:00'}]}},
           'location': {
               'id': 13891,
               'name': 'Ashburton',
               'region': 'Melbourne',
               'state': 'VIC',
               'timeZone': 'Australia/Melbourne',
               'lat': -37.86339,
               'lng': 145.07942,
               'postcode': '3147',
               'typeId': 21}}
    requests.get.return_value = result

    assert client.Weather('api_key', 'loc').get_weather() \
        == client.Weather.Forecast('Partly cloudy. Light winds.', '1-5', 5)

    requests.get.assert_called_with(
        'https://api.willyweather.com.au/v2/api_key/locations/loc/weather.json'
        '?forecasts=rainfall&regionPrecis=true&days=1')


@patch.object(client, 'requests')
def test_get_weather_error(requests):
    requests.get.side_effect = Exception('timeout')

    assert client.Weather('api_key', 'loc').get_weather() \
        == client.Weather.Forecast('Error in get_weather()', '0', 100)

    requests.get.assert_called_with(
        'https://api.willyweather.com.au/v2/api_key/locations/loc/weather.json'
        '?forecasts=rainfall&regionPrecis=true&days=1')


@patch.object(client, 'requests')
def test_get_temperatures(requests):
    result = Mock()
    result.json.return_value \
        = {'regionPrecis': {
            'days': [
                {'dateTime': '2018-02-07 00:00:00',
                 'entries': [
                     {'dateTime': '2018-02-07 00:00:00',
                      'precis': 'Hot and mostly sunny. '
                                'Winds north to northeasterly 15 to 25 km/h '
                                'tending south to southeasterly in the late '
                                'afternoon then becoming light in the late '
                                'evening.'}]}],
            'issueDateTime': '2018-02-07 14:41:14',
            'name': 'Melbourne'},
           'forecasts': {
               'temperature': {
                   'days': [
                       {'dateTime': '2018-02-07 00:00:00',
                        'entries': [
                            {'dateTime': '2018-02-07 00:00:00',
                             'temperature': 21.7},
                            {'dateTime': '2018-02-07 01:00:00',
                             'temperature': 20.6},
                            {'dateTime': '2018-02-07 02:00:00',
                             'temperature': 19.7},
                            {'dateTime': '2018-02-07 03:00:00',
                             'temperature': 18.9},
                            {'dateTime': '2018-02-07 04:00:00',
                             'temperature': 18.4},
                            {'dateTime': '2018-02-07 05:00:00',
                             'temperature': 18.1},
                            {'dateTime': '2018-02-07 06:00:00',
                             'temperature': 17.9},
                            {'dateTime': '2018-02-07 07:00:00',
                             'temperature': 18.1},
                            {'dateTime': '2018-02-07 08:00:00',
                             'temperature': 19.5},
                            {'dateTime': '2018-02-07 09:00:00',
                             'temperature': 21.8},
                            {'dateTime': '2018-02-07 10:00:00',
                             'temperature': 25.2},
                            {'dateTime': '2018-02-07 11:00:00',
                             'temperature': 28.6},
                            {'dateTime': '2018-02-07 12:00:00',
                             'temperature': 30.9},
                            {'dateTime': '2018-02-07 13:00:00',
                             'temperature': 32.7},
                            {'dateTime': '2018-02-07 14:00:00',
                             'temperature': 34.1},
                            {'dateTime': '2018-02-07 15:00:00',
                             'temperature': 35.1},
                            {'dateTime': '2018-02-07 16:00:00',
                             'temperature': 35.7},
                            {'dateTime': '2018-02-07 17:00:00',
                             'temperature': 35.6},
                            {'dateTime': '2018-02-07 18:00:00',
                             'temperature': 34.6},
                            {'dateTime': '2018-02-07 19:00:00',
                             'temperature': 33},
                            {'dateTime': '2018-02-07 20:00:00',
                             'temperature': 31.1},
                            {'dateTime': '2018-02-07 21:00:00',
                             'temperature': 29.1},
                            {'dateTime': '2018-02-07 22:00:00',
                             'temperature': 27.5},
                            {'dateTime': '2018-02-07 23:00:00',
                             'temperature': 26}]}],
                   'issueDateTime': '2018-02-07 14:19:46',
                   'units': {'temperature': 'c'}}},
           'location': {'id': 13891,
                        'lat': -37.86339,
                        'postcode': '3147',
                        'lng': 145.07942,
                        'timeZone': 'Australia/Melbourne',
                        'region': 'Melbourne',
                        'typeId': 21,
                        'name': 'Ashburton',
                        'state': 'VIC'}}

    requests.get.return_value = result

    assert client.Weather('api_key', 'loc').get_temperatures() \
        == [21.7, 20.6, 19.7, 18.9, 18.4, 18.1, 17.9, 18.1, 19.5,
            21.8, 25.2, 28.6, 30.9, 32.7, 34.1, 35.1, 35.7, 35.6,
            34.6, 33, 31.1, 29.1, 27.5, 26]

    requests.get.assert_called_with(
        'https://api.willyweather.com.au/v2/api_key/locations/loc/weather.json'
        '?forecasts=temperature&regionPrecis=true&days=1')


def test_any_rain_y():
    weather = client.Weather('api_key', 'loc')
    with patch.object(weather, 'get_weather') as get_weather:
        get_weather.return_value \
            = client.Weather.Forecast('weather', '10-20', 2)

        assert weather.any_rain(1)


def test_any_rain_n():
    weather = client.Weather('api_key', 'loc')
    with patch.object(weather, 'get_weather') as get_weather:
        get_weather.return_value \
            = client.Weather.Forecast('weather', '0-2', 10)

        assert not weather.any_rain(15)


@patch.object(client, 'requests')
def test_get_week_weather(requests):
    precis = [{'entries': [{'precis': 'Partly cloudy. Light winds.',
                          'dateTime': '2017-08-22 00:00:00'}],
             'dateTime': '2017-08-22 00:00:00'},
            {'entries': [{'precis': 'Very cloudy. High winds.',
                          'dateTime': '2017-08-23 00:00:00'}],
             'dateTime': '2017-08-23 00:00:00'},
            {'entries': [{'precis': 'Clear.',
                          'dateTime': '2017-08-24 00:00:00'}],
             'dateTime': '2017-08-24 00:00:00'}]

    weather = [{"dateTime": "2017-08-22 00:00:00",
                "entries": [{"dateTime": "2017-08-22 00:00:00",
                             "precisCode": "partly-cloudy",
                             "precis": "Partly cloudy",
                             "precisOverlayCode": "",
                             "night": False,
                             "min": 15,
                             "max": 26}]},
               {"dateTime": "2017-08-23 00:00:00",
                "entries": [{"dateTime": "2017-08-23 00:00:00",
                             "precisCode": "very-cloudy",
                             "precis": "Very cloudy",
                             "precisOverlayCode": "",
                             "night": False,
                             "min": 16,
                             "max": 27}]},
               {"dateTime": "2017-08-24 00:00:00",
                "entries": [{"dateTime": "2017-08-24 00:00:00",
                             "precisCode": "clear",
                             "precis": "Clear",
                             "precisOverlayCode": "",
                             "night": False,
                             "min": 17,
                             "max": 28}]}]

    result = Mock()
    result.json.return_value \
        = {'regionPrecis': {
            'name': 'Melbourne Area',
            'issueDateTime': '2017-08-22 18:41:29',
            'days': precis},
           'forecasts': {'weather': {'days': weather,
                                     "units": {"temperature": "c"},
                                     "issueDateTime": "2020-01-01 19:19:42"}},
           'location': {
               'id': 13891,
               'name': 'Ashburton',
               'region': 'Melbourne',
               'state': 'VIC',
               'timeZone': 'Australia/Melbourne',
               'lat': -37.86339,
               'lng': 145.07942,
               'postcode': '3147',
               'typeId': 21}}
    requests.get.return_value = result

    assert client.Weather('api_key', 'loc').get_week_weather() \
        == [client.Weather.DayForecast('Tue', 'Partly cloudy. Light winds.', 15, 26),
            client.Weather.DayForecast('Wed', 'Very cloudy. High winds.', 16, 27),
            client.Weather.DayForecast('Thu', 'Clear.', 17, 28)]

    requests.get.assert_called_with(
        'https://api.willyweather.com.au/v2/api_key/locations/loc/weather.json'
        '?forecasts=weather&regionPrecis=true&days=7')


@patch.object(client, 'requests')
def test_get_week_weather_error(requests):
    requests.get.side_effect = Exception('timeout')

    assert client.Weather('api_key', 'loc').get_week_weather() \
        == [client.Weather.DayForecast('', 'Error in get_week_weather()', 0, 0)]

    requests.get.assert_called_with(
        'https://api.willyweather.com.au/v2/api_key/locations/loc/weather.json'
        '?forecasts=weather&regionPrecis=true&days=7')


def test_radar_url():
    assert client.Radar(['1', '2']).urls(_now='now') \
        == ['http://www.bom.gov.au/radar/1.gif?now',
            'http://www.bom.gov.au/radar/2.gif?now']


def test_radar_url_ftp():
    assert client.Radar(['1', '2'], request_mode='ftp').urls(_now='now') \
        == ['ftp://ftp.bom.gov.au/anon/gen/radar/1.gif',
            'ftp://ftp.bom.gov.au/anon/gen/radar/2.gif']


@pytest.fixture
def img():
    return Image.open(os.path.dirname(os.path.realpath(__file__))
                      + '/test/radar-no-rain.gif').convert('RGB')


def test_rain_pixel_count_box_size(img):
    assert client.Radar.rain_pixel_count(img, box_size=0) == 8414


def test_rain_pixel_count(img):
    assert client.Radar.rain_pixel_count(img) == 8414 - 15 * 25 * 18


def test_rain_pixel_tally(img):
    assert list(client.Radar.rain_pixel_tally(img)) \
        == [1645, 684, 530, 504, 486,
            473, 470, 460, 454, 454,
            450, 450, 450, 450, 454]


@pytest.mark.parametrize('pixels,any_rain', [
    (12345, True),
    (123, False),
    (None, True),
])
def test_any_rain_on_radar(pixels, any_rain):
    radar = client.Radar(['IDR023'])

    with patch.object(radar, 'radar_image') as radar_image:

        if pixels:
            radar_img = Mock()
            radar_img.getdata.return_value = [(0, 216, 195)] * pixels
        else:
            radar_img = None

        radar_image.return_value = radar_img

        assert radar.any_rain(1500, _now='now') == any_rain

        radar_image.assert_called_with(
            ['http://www.bom.gov.au/radar/IDR023.gif?now'])


@pytest.mark.parametrize('pixels,any_rain', [
    (12345, True),
    (123, False),
    (None, True),
])
def test_any_rain_on_radar_ftp(pixels, any_rain):
    radar = client.Radar(['IDR023'], request_mode='ftp')

    with patch.object(radar, 'radar_image') as radar_image:

        if pixels:
            radar_img = Mock()
            radar_img.getdata.return_value = [(0, 216, 195)] * pixels
        else:
            radar_img = None

        radar_image.return_value = radar_img

        assert radar.any_rain(1500, _now='now') == any_rain

        radar_image.assert_called_with(
            ['ftp://ftp.bom.gov.au/anon/gen/radar/IDR023.gif'])
